# Physical Design Flow with open source tools

## Synthesis using [Yosys](http://www.clifford.at/yosys/)

Setup from Yosys [Github](https://github.com/YosysHQ/yosys/blob/master/README.md)

```sh
$ sudo apt-get install build-essential clang bison flex \
	libreadline-dev gawk tcl-dev libffi-dev git \
	graphviz xdot pkg-config python3 libboost-system-dev \
	libboost-python-dev libboost-filesystem-dev
```